package io.pivotal.microservices.products;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Repository for Product data implemented using Spring Data JPA.
 *
 * @author Paul Chapman
 */
public interface ProductsRepository extends Repository<Product, Long> {
    /**
     * Find an account with the specified account number.
     *
     * @param productNumber
     * @return The account if found, null otherwise.
     */
    public Product findByNumber(String productNumber);

    /**
     * Find accounts whose owner name contains the specified string
     *
     * @param partialName Any alphabetic string.
     * @return The list of matching accounts - always non-null, but may be
     * empty.
     */
    public List<Product> findByNameContainingIgnoreCase(String partialName);

    /**
     * Fetch the number of accounts known to the system.
     *
     * @return The number of accounts.
     */
    @Query("SELECT count(*) from Product")
    public int countProduct();
}
