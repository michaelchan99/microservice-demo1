package io.pivotal.microservices.products;

import io.pivotal.microservices.exceptions.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

/**
 * A RESTFul controller for accessing account information.
 *
 * @author Paul Chapman
 */
@RestController
public class ProductsController {

    protected Logger logger = Logger.getLogger(ProductsController.class
            .getName());
    protected ProductsRepository productsRepository;

    /**
     * Create an instance plugging in the respository of Accounts.
     *
     * @param productsRepository An account repository implementation.
     */
    @Autowired
    public ProductsController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;

        logger.info("ProductsRepository says system has "
                + productsRepository.countProduct() + " accounts");
    }

    /**
     * Fetch an account with the specified account number.
     *
     * @param productNumber A numeric, 9 digit account number.
     * @return The account if found.
     * @throws AccountNotFoundException If the number is not recognised.
     */
    @RequestMapping("/products/{productNumber}")
    public Product byNumber(@PathVariable("productNumber") String productNumber) {

        logger.info("products-service byNumber() invoked: " + productNumber);
        Product account = productsRepository.findByNumber(productNumber);
        logger.info("accounts-service byNumber() found: " + account);

        if (account == null)
            throw new AccountNotFoundException(productNumber);
        else {
            return account;
        }
    }

    /**
     * Fetch accounts with the specified name. A partial case-insensitive match
     * is supported. So <code>http://.../accounts/owner/a</code> will find any
     * accounts with upper or lower case 'a' in their name.
     *
     * @param partialName
     * @return A non-null, non-empty set of accounts.
     * @throws AccountNotFoundException If there are no matches at all.
     */
    @RequestMapping("/products/owner/{name}")
    public List<Product> byOwner(@PathVariable("name") String partialName) {
        logger.info("products-service byOwner() invoked: "
                + productsRepository.getClass().getName() + " for "
                + partialName);

        List<Product> accounts = productsRepository
                .findByNameContainingIgnoreCase(partialName);
        logger.info("products-service byOwner() found: " + accounts);

        if (accounts == null || accounts.size() == 0)
            throw new AccountNotFoundException(partialName);
        else {
            return accounts;
        }
    }
}
